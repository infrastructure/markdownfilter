from panflute import *

import re

issue_re = re.compile(r'^(?P<namespace>([A-Za-z0-9_.][-A-Za-z0-9_.]*[-A-Za-z0-9_]?/)*)(?P<project>[A-Za-z0-9_.][-A-Za-z0-9_.]*[-A-Za-z0-9_]?)?#(?P<id>\d+)(?P<tail>.*)')
mr_re = re.compile(r'^(?P<namespace>([A-Za-z0-9_.][-A-Za-z0-9_.]*[-A-Za-z0-9_]?/)*)(?P<project>[A-Za-z0-9_.][-A-Za-z0-9_.]*[-A-Za-z0-9_]?)?!(?P<id>\d+)(?P<tail>.*)')
snippet_re = re.compile(r'^(?P<namespace>([A-Za-z0-9_.][-A-Za-z0-9_.]*[-A-Za-z0-9_]?/)*)(?P<project>[A-Za-z0-9_.][-A-Za-z0-9_.]*[-A-Za-z0-9_]?)?\$(?P<id>\d+)(?P<tail>.*)')
baseurl_re = re.compile(r'^host:(?P<baseurl>.+)')
project_re = re.compile(r'^ref:(?P<namespace>([A-Za-z0-9_.][-A-Za-z0-9_.]*[-A-Za-z0-9_]?/)*)(?P<project>[A-Za-z0-9_.][-A-Za-z0-9_.]*[-A-Za-z0-9_]?)')

escapes = r'\'"_@~%'

def convert_shortlink(doc,match,symbol,url_fragment):
    namespace = match.group('namespace') or ''
    project = match.group('project') or ''
    linktext = ''.join((namespace,project,symbol,match.group('id')))
    url = '{}{}{}/{}/{}'.format(doc.baseurl,namespace or doc.namespace,project or doc.project,url_fragment,match.group('id'))
    link = Link(RawInline(linktext),url=url)

    tail = match.group('tail')
    if tail:
        return [ link, Str(tail) ]
    else:
        return link

def prepare(doc):
    doc.baseurl = None
    doc.namespace = None
    doc.project = None
    doc.dropnext = False

def action(elem, doc):

    if doc.dropnext:
        doc.dropnext = False
        return []

    if isinstance(elem, Str):

        if not doc.project:
            m = project_re.match(elem.text)
            if m:
                doc.namespace = m.group('namespace')
                doc.project = m.group('project')
                doc.dropnext = True
                return []

        m = issue_re.match(elem.text)
        if m:
            return convert_shortlink(doc,m,'#','issues')
        m = mr_re.match(elem.text)
        if m:
            return convert_shortlink(doc,m,'!','merge_requests')
        m = snippet_re.match(elem.text)
        if m:
            return convert_shortlink(doc,m,'$','snippets')
        if any(escape in elem.text for escape in escapes):
            return RawInline(elem.text)
    elif isinstance(elem, Code):
        if not doc.baseurl:
            m = baseurl_re.match(elem.text)
            if m:
                doc.baseurl = m.group('baseurl')
                if not doc.baseurl.endswith('/'):
                    doc.baseurl += '/'
                doc.dropnext = True
                return []
        if not " " in elem.text:
            return RawInline(elem.text)

def main(doc=None):
    return run_filter(action, prepare=prepare, doc=doc)

if __name__ == '__main__':
    main()
